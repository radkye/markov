#include <algorithm>
#include <iostream>
#include <fstream>
#include <regex>
#include <vector>

#include "Common.hpp"
#include "Constants.hpp"
#include "Field.hpp"

const std::vector<std::string> fieldTypeFuncs {std::string("WALL"), std::string("TERMINAL")};

bool isFieldType(const std::string& funcName)
{
    return std::any_of(fieldTypeFuncs.begin(), fieldTypeFuncs.end(),
        [&funcName](const std::string& item)
        {
            return item == funcName;
        });
}

Field::FieldType translateStringToFieldType(const std::string& func)
{
    if (func == std::string("WALL"))
    {
        return Field::FieldType::WALL;
    }
    if (func == std::string("TERMINAL"))
    {
        return Field::FieldType::TERMINAL;
    }
    if (func == std::string("SPECIAL"))
    {
        return Field::FieldType::SPECIAL;
    }
    return Field::FieldType::NORMAL;
}

bool loadEnvironment(Environment& environment, const std::string& filePath)
{
    std::ifstream file;
    file.open(filePath.c_str());
    bool isGridSizeSet = false;
    if (not file.is_open())
    {
        std::cerr << "File: " << filePath << " could not be opened." << std::endl;
        return false;
    }

    while (file.peek() != EOF)
    {
        std::string tempLine;
        std::getline(file, tempLine);
        auto semiColonPos = tempLine.find(";");
        auto func = tempLine.substr(0, semiColonPos);
        if (func == std::string("REWARD"))
        {
            auto rewardValueString = tempLine.substr(semiColonPos + 1, tempLine.length() - semiColonPos);
            environment.setReward(std::stod(rewardValueString));
            continue;
        }
        if (func == std::string("P1"))
        {
            auto probability = tempLine.substr(semiColonPos + 1, tempLine.length() - semiColonPos);
            environment.setProbability1(std::stod(probability));
            continue;
        }
        if (func == std::string("P2"))
        {
            auto probability = tempLine.substr(semiColonPos + 1, tempLine.length() - semiColonPos);
            environment.setProbability2(std::stod(probability));
            continue;
        }
        if (func == std::string("P3"))
        {
            auto probability = tempLine.substr(semiColonPos + 1, tempLine.length() - semiColonPos);
            environment.setProbability3(std::stod(probability));
            continue;
        }
        if (isFieldType(func))
        {
            auto commaPos = tempLine.find(",");
            auto xPos = tempLine.substr(semiColonPos + 1, commaPos - semiColonPos - 1);
            auto yPos = tempLine.substr(commaPos + 1, commaPos - semiColonPos - 1);
            auto semiColonPos2 = tempLine.find(";", semiColonPos + 1);
            auto semiColonPos3 = tempLine.find(";", semiColonPos2 + 1);
            auto terminalValue = tempLine.substr(semiColonPos2 + 1, semiColonPos3 - semiColonPos2 - 1);
            if (terminalValue.empty())
            {
                terminalValue = std::to_string(DEFAULT_TERMINAL_VALUE);
            }
            auto reward = tempLine.substr(semiColonPos3 + 1, tempLine.length() - semiColonPos3);
            environment.setField(translateStringToFieldType(func), std::stoi(xPos), std::stoi(yPos),
                std::stod(terminalValue), std::stod(reward));
            continue;
        }
        if (func == std::string("SPECIAL"))
        {
            auto commaPos = tempLine.find(",");
            auto xPos = tempLine.substr(semiColonPos + 1, commaPos - semiColonPos - 1);
            auto yPos = tempLine.substr(commaPos + 1, commaPos - semiColonPos - 1);
            auto semiColonPos2 = tempLine.find(";", semiColonPos + 1);
            auto semiColonPos3 = tempLine.find(";", semiColonPos2 + 1);
            auto specialStateValue = tempLine.substr(semiColonPos2 + 1, semiColonPos3 - semiColonPos2 - 1);
            if (specialStateValue.empty())
            {
                specialStateValue = std::to_string(DEFAULT_REWARD);
            }
            auto specialValueType = tempLine.substr(semiColonPos3 + 1, tempLine.length() - semiColonPos3);
            environment.setSpecialField(translateStringToFieldType(func), std::stoi(xPos), std::stoi(yPos),
                std::stod(specialStateValue), specialValueType);
            continue;
        }
        if (func == std::string("GRIDSIZE"))
        {
            auto commaPos = tempLine.find(",");
            auto xSize = tempLine.substr(semiColonPos + 1, commaPos - semiColonPos - 1);
            auto ySize = tempLine.substr(commaPos + 1, tempLine.length() - commaPos);
            std::cout << "x: " << xSize << " y: " << ySize << std::endl;
            environment.setupGrid(std::stoi(xSize), std::stoi(ySize));
            isGridSizeSet = true;
            continue;
        }
        if (func == std::string("GAMMA"))
        {
            auto gammaValueString = tempLine.substr(semiColonPos + 1, tempLine.length() - semiColonPos);
            environment.setGamma(std::stod(gammaValueString));
            continue;
        }
        if (isGridSizeSet)
        {
            std::cout << isGridSizeSet << std::endl;
        }
    }

    return true;
}

std::string getFieldsIcon(const Field::FieldType& type)
{
    switch (type)
    {
        case Field::FieldType::NORMAL:
            return std::string("N");
        case Field::FieldType::START:
            return std::string("N");
        case Field::FieldType::WALL:
            return std::string("W");
        case Field::FieldType::TERMINAL:
            return std::string("T");
        case Field::FieldType::SPECIAL:
            return std::string("S");
    }
    return std::string("");
}
