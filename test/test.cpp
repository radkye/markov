#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "Direction.cpp"
#include "Field.cpp"

TEST_CASE( "Utility are updated only for chosen direction", "[Field]" ) {
    Field field;
    field.updateUtility(Direction::LEFT, 2);

    auto leftResult = field.getUtility(Direction::LEFT);
    auto upResult = field.getUtility(Direction::UP);
    REQUIRE( leftResult == 2 );
    REQUIRE( upResult == 0 );
}

TEST_CASE( "Field are updated properly and maximum utility is being returned", "[Field]" ) {
    Field field;
    field.updateUtility(Direction::LEFT, 2);
    field.updateUtility(Direction::RIGHT, 3);
    field.updateUtility(Direction::UP, 5);
    field.updateUtility(Direction::DOWN, -1);

    auto result = field.maxUtility();
    REQUIRE( result.first == Direction::UP );
}