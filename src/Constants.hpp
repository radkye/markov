#pragma once

constexpr double DEFAULT_GAMMA = 0.99;
constexpr double DEFAULT_REWARD = 0.04;
constexpr double DEFAULT_TERMINAL_VALUE = 0;
