#pragma once

#include <string>

#include "Environment.hpp"

bool loadEnvironment(Environment& environment, const std::string& filePath);

std::string getFieldsIcon(const Field::FieldType& type);
