#include <iostream>

#include "Direction.hpp"

std::ostream& operator<<(std::ostream& stream, const Direction& direction)
{
    switch (direction)
    {
        case Direction::LEFT:
            stream << "<";
            return stream;
        case Direction::RIGHT:
            stream << ">";
            return stream;
        case Direction::UP:
            stream << "^";
            return stream;
        case Direction::DOWN:
            stream << "v";
            return stream;
    }
    stream << "";
    return stream;
}