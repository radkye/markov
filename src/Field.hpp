#pragma once

#include <map>

#include "Constants.hpp"
#include "Direction.hpp"

struct Utility
{
    Utility();
    Utility(double value);
    Utility(const Utility&) = default;
    Utility& operator=(const Utility&) = default;
    ~Utility() = default;
    std::map<Direction, double> utilities_;
};

class Field
{
public:
    enum class FieldType
    {
        NORMAL = 0,
        START,
        TERMINAL,
        WALL,
        SPECIAL
    };

    std::pair<Direction, double> maxUtility() const;
    std::pair<Direction, double> maxPreviousUtility() const;
    double getUtility(const Direction& direction) const;
    double getReward() const;
    double getPreviousUtility(const Direction& direction) const;
    void updateUtility(const Direction& direction, double value);
    void saveUtilities();
    void setTerminalUtilities(double utility);
    FieldType type() const;
    double terminalValue() const;
    void setType(const FieldType& type);
    void setReward(double reward);
    void setTerminalFieldValue(double terminalValue);
private:
    Utility utility_;
    Utility previousUtility_;
    FieldType type_;
    double terminalValue_ = DEFAULT_TERMINAL_VALUE;
    double reward_ = DEFAULT_REWARD;
};

std::string fieldTypeToString(const Field::FieldType& type);
