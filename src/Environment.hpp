#pragma once

#include <map>
#include <string>
#include <vector>

#include "Direction.hpp"
#include "Field.hpp"

class Environment
{
public:
    void setupGrid(const int xSize, const int ySize);
    void setGamma(const double value);
    void setProbability1(const double value);
    void setProbability2(const double value);
    void setProbability3(const double value);
    void setReward(const double value);
    void setField(const Field::FieldType& type, unsigned xPos, unsigned yPos, double terminalFieldValue,
        double reward);
    void setSpecialField(const Field::FieldType& type, unsigned xPos, unsigned yPos, double terminalFieldValue,
        const std::string& specialStateFunc);
    void printFieldTypes() const;
    void printBestPolicy() const;
    void printBestUtilities() const;
    void printPreviousBestUtilities() const;
    int performReinforcedLearning(double error, const std::string& filePath);
private:
    void saveGrid();
    void setTerminalField(unsigned xPos, unsigned yPos, double terminalFieldValue, double reward);
    void setWall(unsigned xPos, unsigned yPos, double reward);
    double calculateError() const;
    void setEnvironmentalReward(double reward);
    void updateUtilityValuesAccordingToMoves(int yPos, int xPos);

    std::vector<std::vector<Field>> grid_;
    bool isGridSet_ = false;
    bool isEnvironmentalRewardSet_ = false;
    double mainChance_ = 0.8;
    double sideChance_ = 0.1;
    double sideChance2_ = 0.1;
    double backChance_ = 0;
    bool p1set_ = false;
    bool p2set_ = false;
    bool p3set_ = false;
    bool areChancesLoadedFromFile = false;
    double gamma_;
    double reward_ = DEFAULT_REWARD;
};