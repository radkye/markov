#include "Field.hpp"

Utility::Utility()
{
    utilities_[Direction::LEFT] = double();
    utilities_[Direction::RIGHT] = double();
    utilities_[Direction::UP] = double();
    utilities_[Direction::DOWN] = double();
}

Utility::Utility(double value)
{
    utilities_[Direction::LEFT] = value;
    utilities_[Direction::RIGHT] = value;
    utilities_[Direction::UP] = value;
    utilities_[Direction::DOWN] = value;
}

std::pair<Direction, double> Field::maxUtility() const
{
    std::pair<Direction, double> result = std::make_pair(Direction::UP, -1);
    for (auto&& item : utility_.utilities_)
    {
        if (item.second > result.second)
        {
            result = item;
        }
    }
    return result;
}

std::pair<Direction, double> Field::maxPreviousUtility() const
{
    std::pair<Direction, double> result = std::make_pair(Direction::UP, -1);
    for (auto&& item : previousUtility_.utilities_)
    {
        if (item.second > result.second)
        {
            result = item;
        }
    }
    return result;
}

void Field::updateUtility(const Direction& direction, double value)
{
    utility_.utilities_.at(direction) = value;
}

double Field::getUtility(const Direction& direction) const
{
    return utility_.utilities_.at(direction);
}

double Field::getReward() const
{
    return reward_;
}

double Field::getPreviousUtility(const Direction& direction) const
{
    return previousUtility_.utilities_.at(direction);
}

void Field::setReward(double reward)
{
    std::cout << "Setting reward: " << reward << std::endl;
    reward_ = reward;
}

void Field::setType(const FieldType& type)
{
    type_ = type;
}

void Field::setTerminalFieldValue(double terminalValue)
{
    terminalValue_ = terminalValue;
}

std::string fieldTypeToString(const Field::FieldType& type)
{
    if (type == Field::FieldType::TERMINAL)
    {
        return std::string("TERMINAL");
    }
    if (type == Field::FieldType::WALL)
    {
        return std::string("WALL");
    }
    return std::string("");
}

Field::FieldType Field::type() const
{
    return type_;
}

double Field::terminalValue() const
{
    return terminalValue_;
}

void Field::setTerminalUtilities(double utility)
{
    utility_.utilities_[Direction::UP] = utility;
    utility_.utilities_[Direction::DOWN] = utility;
    utility_.utilities_[Direction::LEFT] = utility;
    utility_.utilities_[Direction::RIGHT] = utility;
    std::cout << "Field utility: " << utility_.utilities_[Direction::UP] << std::endl;
}

void Field::saveUtilities()
{
    previousUtility_ = utility_;
}
