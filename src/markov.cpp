#include <algorithm>
#include <iostream>
#include <map>
#include "Common.hpp"
#include "Direction.hpp"
#include "Field.hpp"
#include "Environment.hpp"

int main(int argc, char** argv)
{
    if (argc < 3)
    {
        std::cout << "Wrong call of the program!" << std::endl;
        std::cout << "To run the program run command:" << std::endl;
        std::cout << "markov <path to input file (environemnt settings)> <path to output file (for error convergence data)>"
            << std::endl;
        return 0;
    }
    std::string inputFilePath(argv[1]);
    std::string outputFilePath(argv[2]);
    Environment env;
    if (!loadEnvironment(env, inputFilePath))
    {
        std::cout << "I failed :(" << std::endl;
        return 1;
    }
    env.printFieldTypes();
    env.printBestPolicy();
    env.performReinforcedLearning(0.0001, outputFilePath);
    return 0;
}