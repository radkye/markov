#include <iostream>
#include <fstream>
#include <cmath>

#include "Common.hpp"
#include "Environment.hpp"

void Environment::setupGrid(const int xSize, const int ySize)
{
    grid_.resize(xSize);
    for (auto&& column : grid_)
    {
        column.resize(ySize);
    }
    isGridSet_ = true;
    std::cout << "Grid size has been set as: " << xSize << "x" << ySize << std::endl;
}

void Environment::setGamma(const double value)
{
    gamma_ = value;
    std::cout << "GAMMA has been set to: " << gamma_ << std::endl;
}

void Environment::setProbability1(const double value)
{
    areChancesLoadedFromFile = true;
    p1set_ = true;
    mainChance_ = value;
    std::cout << "P1 has been set to: " << mainChance_ << std::endl;
    if (p1set_ && p2set_ && p3set_)
    {
        backChance_ = 1 - mainChance_ - sideChance_ - sideChance2_;
        std::cout << "P4 has been set to: " << backChance_ << std::endl;
    }
}
void Environment::setProbability2(const double value)
{
    areChancesLoadedFromFile = true;
    p2set_ = true;
    sideChance_ = value;
    std::cout << "P2 has been set to: " << sideChance_ << std::endl;
    if (p1set_ && p2set_ && p3set_)
    {
        backChance_ = 1 - mainChance_ - sideChance_ - sideChance2_;
        std::cout << "P4 has been set to: " << backChance_ << std::endl;
    }
}
void Environment::setProbability3(const double value)
{
    areChancesLoadedFromFile = true;
    p3set_ = true;
    sideChance2_ = value;
    std::cout << "P3 has been set to: " << sideChance2_ << std::endl;
    if (p1set_ && p2set_ && p3set_)
    {
        backChance_ = 1 - mainChance_ - sideChance_ - sideChance2_;
        std::cout << "P4 has been set to: " << backChance_ << std::endl;
    }
}

void Environment::setReward(const double value)
{
    reward_ = value;
    for (int i = 0; i < grid_.size(); i++)
    {
        for (int j = 0; j < grid_[i].size(); j++)
        {
            grid_[i][j].setReward(value);
        }
    }
    std::cout << "REWARD has been set to: " << reward_ << std::endl;
}

void Environment::setField(const Field::FieldType& type, unsigned xPos, unsigned yPos, double terminalFieldValue,
    double reward)
{
    if (!isGridSet_)
    {
        std::cout << "Fix file format. GRIDSIZE field should be specified before field types!" << std::endl;
        return;
    }
    if (xPos > grid_.size() - 1 || yPos > grid_.front().size() - 1)
    {
        std::cout << "Wrong position indexes for setting field type in grid." << std::endl;
        return;
    }
    if (isEnvironmentalRewardSet_)
    {
        reward = reward_;
    }
    if (type == Field::FieldType::TERMINAL)
    {
        setTerminalField(xPos, yPos, terminalFieldValue, reward);
        return;
    }
    if (type == Field::FieldType::WALL)
    {
        setWall(xPos, yPos, reward);
        return;
    }
}

void Environment::setSpecialField(const Field::FieldType& type, unsigned xPos, unsigned yPos,
    double specialFieldValue, const std::string& specialStateFunc)
{
    grid_[xPos][yPos].setType(type);
    if (specialStateFunc == std::string("REWARD"))
    {
        grid_[xPos][yPos].setReward(specialFieldValue);
    }
    else
    {
        std::cout << "Setting special state utility" << std::endl;
        grid_[xPos][yPos].setTerminalUtilities(specialFieldValue);
    }
    std::cout << "Setting up SPECIAL field: " << xPos << "x" << yPos << " with specialFieldValue: "
        << specialFieldValue << " of type: " << specialStateFunc << std::endl;
}

void Environment::setEnvironmentalReward(double reward)
{
    isEnvironmentalRewardSet_ = true;
    reward_ = reward;
    if (isGridSet_)
    {
        for (auto&& row : grid_)
        {
            for (auto&& field : row)
            {
                field.setReward(reward_);
            }
        }
    }
}

void Environment::setTerminalField(unsigned xPos, unsigned yPos, double terminalFieldValue, double reward)
{
    grid_[xPos][yPos].setType(Field::FieldType::TERMINAL);
    grid_[xPos][yPos].setReward(reward);
    grid_[xPos][yPos].setTerminalFieldValue(terminalFieldValue);
    grid_[xPos][yPos].setTerminalUtilities(terminalFieldValue);
    std::cout << "Setting up TERMINAL field: " << xPos << "x" << yPos << " with terminalStateValue: "
        << terminalFieldValue << " and reward: " << reward << std::endl;
}

void Environment::setWall(unsigned xPos, unsigned yPos, double reward)
{
    grid_[xPos][yPos].setType(Field::FieldType::WALL);
    grid_[xPos][yPos].setReward(reward);
    std::cout << "Setting up WALL field: " << xPos << "x" << yPos << " with reward: "
        << reward << std::endl;
}

void Environment::printFieldTypes() const
{
    for (unsigned i = 0; i < grid_.size(); i++)
    {
        for (unsigned j = 0; j < grid_[i].size(); j++)
        {
            std::cout << "| " << getFieldsIcon(grid_[i][j].type()) << " ";
        }
        std::cout << "|" << std::endl;
    }
}

void Environment::printBestUtilities() const
{
    for (unsigned i = 0; i < grid_.size(); i++)
    {
        for (unsigned j = 0; j < grid_[i].size(); j++)
        {
            std::cout << "| " << grid_[i][j].maxUtility().second << " ";
        }
        std::cout << "|" << std::endl;
    }
}

void Environment::printPreviousBestUtilities() const
{
    for (unsigned i = 0; i < grid_.size(); i++)
    {
        for (unsigned j = 0; j < grid_[i].size(); j++)
        {
            std::cout << "| " << grid_[i][j].maxPreviousUtility().second << " ";
        }
        std::cout << "|" << std::endl;
    }
}

void Environment::printBestPolicy() const
{
    for (unsigned i = 0; i < grid_.size(); i++)
    {
        for (unsigned j = 0; j < grid_[i].size(); j++)
        {
            if (grid_[i][j].type() == Field::FieldType::TERMINAL)
            {
                std::cout << "| " << grid_[i][j].terminalValue() << " ";
                continue;
            }
            if (grid_[i][j].type() == Field::FieldType::WALL)
            {
                std::cout << "| " << " " << " ";
                continue;
            }
            std::cout << "| " << grid_[i][j].maxUtility().first << " ";
        }
        std::cout << "|" << std::endl;
    }
}

double Environment::calculateError() const
{
    double actualError = 0;
    for (unsigned i = 0; i < grid_.size(); i++)
    {
        for (unsigned j = 0; j < grid_[i].size(); j++)
        {
            // std::cout << "Previous: " << grid_[i][j].maxPreviousUtility().second << std::endl;
            // std::cout << "Actual: " << grid_[i][j].maxUtility().second << std::endl;
            if (grid_[i][j].type() == Field::FieldType::NORMAL ||
                grid_[i][j].type() == Field::FieldType::SPECIAL)
            {
                actualError += (actualError + std::abs(grid_[i][j].maxUtility().second 
                    - grid_[i][j].maxPreviousUtility().second));
            }
        }
    }
    return actualError;
}

int Environment::performReinforcedLearning(double approvableError, const std::string& filePath)
{
    double actualError = 0;
    std::vector<int> steps;
    std::vector<double> errorSet;

    int step = 0;
    do
    {
        saveGrid();
        for (unsigned i = 0; i < grid_.size(); i++)
        {
            for (unsigned j = 0; j < grid_[i].size(); j++)
            {
                if (grid_[i][j].type() == Field::FieldType::NORMAL ||
                    grid_[i][j].type() == Field::FieldType::SPECIAL)
                {
                    updateUtilityValuesAccordingToMoves(i, j);
                }
            }
        }
        step++;
        std::cout << "====================================================================================="
            << std::endl;
        std::cout << step << std::endl;
        printPreviousBestUtilities();
        printBestUtilities();
        printBestPolicy();

        actualError = calculateError();
        errorSet.push_back(actualError);
        steps.push_back(step);

    } while (actualError > approvableError);

    std::ofstream outputFile;
    outputFile.open(filePath);

    for (auto&& number : steps)
    {
        outputFile << number << ", ";
    }
    outputFile << std::endl;
    for (auto&& number : errorSet)
    {
        outputFile << number << ", ";
    }

    return step;
}

void Environment::updateUtilityValuesAccordingToMoves(int yPos, int xPos)
{
    auto mainUtilityValue = grid_[yPos][xPos].maxPreviousUtility().second;
    auto utilityValue1 = grid_[yPos][xPos].maxPreviousUtility().second;
    auto utilityValue2 = grid_[yPos][xPos].maxPreviousUtility().second;
    auto backUtilityValue = grid_[yPos][xPos].maxPreviousUtility().second;

    // going UP

    if (yPos - 1 >= 0 && grid_[yPos-1][xPos].type() != Field::FieldType::WALL)
    {
        if (grid_[yPos-1][xPos].type() != Field::FieldType::WALL)
            mainUtilityValue = grid_[yPos-1][xPos].maxPreviousUtility().second;
    }
    if (xPos - 1 >= 0 && grid_[yPos][xPos-1].type() != Field::FieldType::WALL)
        utilityValue1 = grid_[yPos][xPos-1].maxPreviousUtility().second;
    if (xPos + 1 < grid_[yPos].size() && grid_[yPos][xPos+1].type() != Field::FieldType::WALL)
        utilityValue2 = grid_[yPos][xPos+1].maxPreviousUtility().second;
    if (yPos + 1 < grid_.size())
    {
        if (grid_[yPos+1][xPos].type() != Field::FieldType::WALL)
            backUtilityValue = grid_[yPos+1][xPos].maxPreviousUtility().second;
    }

    auto newUpValue = gamma_ * (mainUtilityValue * mainChance_ + utilityValue1 * sideChance_
        + utilityValue2 * sideChance_ + backUtilityValue * backChance_) + grid_[yPos][xPos].getReward();
    grid_[yPos][xPos].updateUtility(Direction::UP, newUpValue);

    // going DOWN

    mainUtilityValue = grid_[yPos][xPos].maxPreviousUtility().second;
    utilityValue1 = grid_[yPos][xPos].maxPreviousUtility().second;
    utilityValue2 = grid_[yPos][xPos].maxPreviousUtility().second;
    backUtilityValue = grid_[yPos][xPos].maxPreviousUtility().second;

    if (yPos + 1 < grid_.size() && grid_[yPos+1][xPos].type() != Field::FieldType::WALL)
        mainUtilityValue = grid_[yPos+1][xPos].maxPreviousUtility().second;
    if (xPos - 1 >= 0 && grid_[yPos][xPos-1].type() != Field::FieldType::WALL)
        utilityValue1 = grid_[yPos][xPos-1].maxPreviousUtility().second;
    if (xPos + 1 < grid_[yPos].size() && grid_[yPos][xPos+1].type() != Field::FieldType::WALL)
        utilityValue2 = grid_[yPos][xPos+1].maxPreviousUtility().second;
    if (yPos - 1 >= 0)
    {
        if (grid_[yPos-1][xPos].type() != Field::FieldType::WALL)
            backUtilityValue = grid_[yPos-1][xPos].maxPreviousUtility().second;
    }

    auto newDownValue = gamma_ * (mainUtilityValue * mainChance_ + utilityValue1 * sideChance_
        + utilityValue2 * sideChance_ + backUtilityValue * backChance_) + grid_[yPos][xPos].getReward();
    grid_[yPos][xPos].updateUtility(Direction::DOWN, newDownValue);

    // going RIGHT

    mainUtilityValue = grid_[yPos][xPos].maxPreviousUtility().second;
    utilityValue1 = grid_[yPos][xPos].maxPreviousUtility().second;
    utilityValue2 = grid_[yPos][xPos].maxPreviousUtility().second;
    backUtilityValue = grid_[yPos][xPos].maxPreviousUtility().second;

    if (xPos + 1 < grid_[yPos].size() && grid_[yPos][xPos+1].type() != Field::FieldType::WALL)
        mainUtilityValue = grid_[yPos][xPos+1].maxPreviousUtility().second;
    if (yPos - 1 >= 0 && grid_[yPos-1][xPos].type() != Field::FieldType::WALL)
        utilityValue1 = grid_[yPos-1][xPos].maxPreviousUtility().second;
    if (yPos + 1 < grid_.size() && grid_[yPos+1][xPos].type() != Field::FieldType::WALL)
        utilityValue2 = grid_[yPos+1][xPos].maxPreviousUtility().second;
    if (xPos - 1 >= 0)
    {
        if (grid_[yPos][xPos - 1].type() != Field::FieldType::WALL)
            backUtilityValue = grid_[yPos][xPos-1].maxPreviousUtility().second;
    }

    auto newRightValue = gamma_ * (mainUtilityValue * mainChance_ + utilityValue1 * sideChance_
        + utilityValue2 * sideChance_ + backUtilityValue * backChance_) + grid_[yPos][xPos].getReward();
    grid_[yPos][xPos].updateUtility(Direction::RIGHT, newRightValue);

    // going LEFT

    mainUtilityValue = grid_[yPos][xPos].maxPreviousUtility().second;
    utilityValue1 = grid_[yPos][xPos].maxPreviousUtility().second;
    utilityValue2 = grid_[yPos][xPos].maxPreviousUtility().second;
    backUtilityValue = grid_[yPos][xPos].maxPreviousUtility().second;

    if (xPos - 1 >= 0 && grid_[yPos][xPos-1].type() != Field::FieldType::WALL)
        mainUtilityValue = grid_[yPos][xPos-1].maxPreviousUtility().second;
    if (yPos - 1 >= 0 && grid_[yPos-1][xPos].type() != Field::FieldType::WALL)
        utilityValue1 = grid_[yPos-1][xPos].maxPreviousUtility().second;
    if (yPos + 1 < grid_.size() && grid_[yPos+1][xPos].type() != Field::FieldType::WALL)
        utilityValue2 = grid_[yPos+1][xPos].maxPreviousUtility().second;
    if (xPos + 1 < grid_[yPos].size())
    {
        if (grid_[yPos][xPos+1].type() != Field::FieldType::WALL)
            backUtilityValue = grid_[yPos][xPos+1].maxPreviousUtility().second;
    }


    auto newLeftValue = gamma_ * (mainUtilityValue * mainChance_ + utilityValue1 * sideChance_
        + utilityValue2 * sideChance_ + backUtilityValue * backChance_) + grid_[yPos][xPos].getReward();
    grid_[yPos][xPos].updateUtility(Direction::LEFT, newLeftValue);
}

void Environment::saveGrid()
{
    for (unsigned i = 0; i < grid_.size(); i++)
    {
        for (unsigned j = 0; j < grid_[i].size(); j++)
        {
            grid_[i][j].saveUtilities();
        }
    }
}
