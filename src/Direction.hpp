#pragma once

#include <iostream>

enum class Direction
{
    LEFT = 0,
    RIGHT,
    UP,
    DOWN
};

std::ostream& operator<<(std::ostream& stream, const Direction& direction);
